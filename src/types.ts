export type Post = {
  id: number;
  content: string;
  updatedAt: string;
  createdAt: string;
};

export type CreatePostDto = {
  content: string;
};

export type UpdatePostDto = {
  content: string;
};
