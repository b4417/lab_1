import bodyParser from 'body-parser';
import express from 'express';
import { getAllPosts, getOnePost, update, deleteOne, createPost } from './persistence';
import { CreatePostDto, UpdatePostDto } from './types';
import _ from 'lodash';

const PORT = 3000;

/**
 * Создаем Express-сервер
 */
export const app = express();

/**
 * Это мидлвара, которая позволяет читать BODY, который передали как JSON-объект
 */
app.use(bodyParser.json());

/**
 * Пример одного эдпоинта, используйте его как образец
 */
app.get('/posts', async (_, res) => {
  const posts = await getAllPosts();

  res.json(posts);
});

app.get('/posts/:id', async (req, res) => {
  const post = await getOnePost(req.params.id);
  
  if(_.isEmpty(post)) {
    res.status(404).json(`Post ${req.params.id} doesn't exists`);
  }

  res.json(post);
});

app.post('/posts', async (req, res) => {
  const reqBody: CreatePostDto = req.body;
  const post = await createPost(reqBody);

  res.json(post);
});

app.patch('/posts/:id', async (req, res) => {
  const reqBody: UpdatePostDto = req.body;
  const newPosts = await update(req.params.id, reqBody);

  res.json(newPosts);
});

app.delete('/posts/:id', async (req, res) => {
  const postsWithoutPost = await deleteOne(req.params.id);

  res.json(postsWithoutPost)
});

/**
 * Чтобы во время тестов ваше запущенное приложение не конфликтовало
 *  с тем, которое поднимается для тестов
 */
if (process.env.NODE_ENV !== 'test') {
  /**
   * Обратите внимание что делает этот метод -- мы подписываемся на весь трафик
   *  с порта 3000, "слушаем его"
   */
  app.listen(PORT, () => {
    console.log(`Express application started on http://localhost:${PORT}`);
  });
}
