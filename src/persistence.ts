import { readFile, writeFile } from 'fs/promises';
import { DB_PATH } from './constants';
import { Post, CreatePostDto, UpdatePostDto } from './types';

export async function getAllPosts(): Promise<Post[]> {
  try {
    const file = await readFile(DB_PATH);
    const fileContent = file.toString();

    if (!fileContent) {
      return [];
    }

    const posts: Post[] = JSON.parse(fileContent);

    return posts;
  } catch (e) {
    throw e;
  }
}
export async function getOnePost(id: string): Promise<Post> {
  const idNumber = Number(id);
  const allPosts: Post[] = await getAllPosts();
  const onePost: Post = allPosts.filter(post => post.id === idNumber)[0];
  return onePost;
}

export async function createPost(postDto: CreatePostDto): Promise<Post> {
  try {
    const posts: Post[] = await getAllPosts();
    const post: Post = {
      id: posts.length + 1,
      ...postDto,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
    }
    posts.push(post);
    
    await writeFile(DB_PATH, JSON.stringify(posts));

    return post;
  } catch (e) {
    throw e;
  }
}

export async function update(id: string, postDto: UpdatePostDto): Promise<Post> {
  try {
    const posts: Post[] = await getAllPosts();
    if (posts === []) {
      return {} as Post;
    }
    const newPosts: Post[] = posts.map(post => {
      if(post.id === Number(id)) {
        return {
          id: Number(id),
          createdAt: post.createdAt,
          ...postDto,
          updatedAt: new Date().toISOString(),
        } as Post;
      }
      return post;
    });

    await writeFile(DB_PATH, JSON.stringify(newPosts));

    return newPosts.filter(post => post.id === Number(id))[0];
  } catch (e) {
    throw e;
  }
}

export async function deleteOne(id: string): Promise<boolean> {
  try {
    const posts: Post[] = await getAllPosts();
    if (posts === []) {
      return false;
    }
    const newPosts: Post[] = posts.filter(post => post.id !== Number(id));

    await writeFile(DB_PATH, JSON.stringify(newPosts));

    return true;
  } catch (e) {
    throw e;
  }
}
